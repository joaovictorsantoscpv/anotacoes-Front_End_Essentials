# Front-End - Essentials
Este documento se trata de anotações pessoais feitas por mim e outros pontos também discutidos no curso ministrado pelo Danilo Vaz, na HyperClass.
# Sumário
1.[ O que é Front-End ?](https://github.com/user/repo/blob/branch/other_file.md)
# 1. O que é Front-End ?
<i>The goal of a front-end developer is to create clear, easy, fast pages and interfaces that will make people understand and care about the information, by putting it in context, expose its legitimacy or lack thereof, and reveal their implicit or explicit interconnection. - The Guardian</i>